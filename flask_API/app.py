#!flask/bin/python

from flask import Flask, render_template, jsonify, request, abort
from flask_restful import Api, Resource, reqparse
import requests
import dash_bootstrap_components as dbc
import os
from flask import send_from_directory


#import dash
#import dash_html_components as html

app = Flask(__name__, static_url_path="")
api = Api(app)

# Setting the location for the sqlite database
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///base.db'
# Adding the configurations for the database
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['PROPAGATE_EXCEPTIONS'] = True

# Import necessary classes from base.py
from flask_API.base import db, Tasks

db.init_app(app)
app.app_context().push()

# Create the databases
db.create_all()


@app.route('/')
def home():
    return render_template('home.html')

@app.route('/predictor/')
def predictor():
    return render_template('NN.html')

@app.route('/about/')
def about():
    return render_template('about.html')

@app.route('/api/')
def api_docs():
    return render_template('api.html')

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'), 'favicon.ico', mimetype='image/vnd.microsoft.icon')

class TaskAPI(Resource):

    def __init__(self):
        super(TaskAPI, self).__init__()

    def get(self):
            return {'Tasks': list(map(lambda x: x.json(), Tasks.query.all()))}

    def delete(self):
        return {'Tasks': list(map(lambda x: x.delete_(), Tasks.query.all()))}

class SingleTaskAPI(Resource):
    parser = reqparse.RequestParser()

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('tweet_id', type=int, location='json')
        self.reqparse.add_argument('full_text', type=str, location='json')
        self.reqparse.add_argument('screen_name', type=str, location='json')
        self.reqparse.add_argument('followers_count', type=int, location='json')
        self.reqparse.add_argument('created_at', type=str, location='json')
        self.reqparse.add_argument('VADER_sentiment', type=float, location='json')
        self.reqparse.add_argument('date', type=str, location='json')
        self.reqparse.add_argument('year', type=str, location='json')
        self.reqparse.add_argument('bioguide', type=str, location='json')
        self.reqparse.add_argument('twitter_id', type=int, location='json')
        self.reqparse.add_argument('religion', type=str, location='json')
        self.reqparse.add_argument('official_full_name', type=str, location='json')
        self.reqparse.add_argument('gender', type=str, location='json')
        self.reqparse.add_argument('birthday', type=str, location='json')
        self.reqparse.add_argument('party', type=str, location='json')
        self.reqparse.add_argument('state', type=str, location='json')
        self.reqparse.add_argument('type', type=str, location='json')

        super(SingleTaskAPI, self).__init__()

    def get(self, date):
        return {'Tweets': list(map(lambda x: x.json(), Tasks.find_by_date(date)))}  # Functional

    def put(self):
        pass

    def post(self, date):
        args = self.reqparse.parse_args()
        print(args)
        item = Tasks(tweet_id=args['tweet_id'], full_text=args['full_text'], screen_name=args['screen_name'],
                     followers_count=args['followers_count'],
                     created_at=args['created_at'],
                     VADER_sentiment=args['VADER_sentiment'], date=args['date'], year=args['year'],
                     bioguide=args['bioguide'], twitter_id=args['twitter_id'], religion=args['religion'],
                     official_full_name=args['official_full_name'], gender=args['gender'],
                     birthday=args['birthday'], party=args['party'], state=args['state'], type=args['type'])

        item.save_to()
        return item.json()  # Functional

    def delete(self, date):
        return {'Tasks': list(map(lambda x: x.delete_(), Tasks.find_by_date(date)))}

class SingleTaskByID(Resource):
    parser = reqparse.RequestParser()

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('tweet_id', type=int, location='json')
        self.reqparse.add_argument('full_text', type=str, location='json')
        self.reqparse.add_argument('screen_name', type=str, location='json')
        self.reqparse.add_argument('followers_count', type=int, location='json')
        self.reqparse.add_argument('created_at', type=str, location='json')
        self.reqparse.add_argument('VADER_sentiment', type=float, location='json')
        self.reqparse.add_argument('date', type=str, location='json')
        self.reqparse.add_argument('year', type=str, location='json')
        self.reqparse.add_argument('bioguide ', type=str, location='json')
        self.reqparse.add_argument('twitter_id', type=int, location='json')
        self.reqparse.add_argument('religion', type=str, location='json')
        self.reqparse.add_argument('official_full_name', type=str, location='json')
        self.reqparse.add_argument('gender', type=str, location='json')
        self.reqparse.add_argument('birthday', type=str, location='json')
        self.reqparse.add_argument('party', type=str, location='json')
        self.reqparse.add_argument('state', type=str, location='json')
        self.reqparse.add_argument('type', type=str, location='json')


        super(SingleTaskByID, self).__init__()

    def get(self, tweet_id):
        return {'Tweets': list(map(lambda x: x.json(), Tasks.find_by_id(tweet_id)))}  # Functional

    """def put(self, id):
        todos[todo_id] = request.form['data']
        return {todo_id: todos[todo_id]}
        
        """

    def delete(self, tweet_id):
        return {'Tweets': list(map(lambda x: x.delete_(), Tasks.find_by_id(tweet_id)))}

class TweetByState(Resource):
    parser = reqparse.RequestParser()

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('tweet_id', type=int, location='json')
        self.reqparse.add_argument('full_text', type=str, location='json')
        self.reqparse.add_argument('screen_name', type=str, location='json')
        self.reqparse.add_argument('followers_count', type=int, location='json')
        self.reqparse.add_argument('created_at', type=str, location='json')
        self.reqparse.add_argument('VADER_sentiment', type=float, location='json')
        self.reqparse.add_argument('date', type=str, location='json')
        self.reqparse.add_argument('year', type=str, location='json')
        self.reqparse.add_argument('bioguide ', type=str, location='json')
        self.reqparse.add_argument('twitter_id', type=int, location='json')
        self.reqparse.add_argument('religion', type=str, location='json')
        self.reqparse.add_argument('official_full_name', type=str, location='json')
        self.reqparse.add_argument('gender', type=str, location='json')
        self.reqparse.add_argument('birthday', type=str, location='json')
        self.reqparse.add_argument('party', type=str, location='json')
        self.reqparse.add_argument('state', type=str, location='json')
        self.reqparse.add_argument('type', type=str, location='json')


        super(TweetByState, self).__init__()

    def get(self, state):
        return {'Tweets': list(map(lambda x: x.json(), Tasks.find_by_state(state)))}  # Functional

class TweetByScreenName(Resource):
    parser = reqparse.RequestParser()

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('tweet_id', type=int, location='json')
        self.reqparse.add_argument('full_text', type=str, location='json')
        self.reqparse.add_argument('screen_name', type=str, location='json')
        self.reqparse.add_argument('followers_count', type=int, location='json')
        self.reqparse.add_argument('created_at', type=str, location='json')
        self.reqparse.add_argument('VADER_sentiment', type=float, location='json')
        self.reqparse.add_argument('date', type=str, location='json')
        self.reqparse.add_argument('year', type=str, location='json')
        self.reqparse.add_argument('bioguide ', type=str, location='json')
        self.reqparse.add_argument('twitter_id', type=int, location='json')
        self.reqparse.add_argument('religion', type=str, location='json')
        self.reqparse.add_argument('official_full_name', type=str, location='json')
        self.reqparse.add_argument('gender', type=str, location='json')
        self.reqparse.add_argument('birthday', type=str, location='json')
        self.reqparse.add_argument('party', type=str, location='json')
        self.reqparse.add_argument('state', type=str, location='json')
        self.reqparse.add_argument('type', type=str, location='json')


        super(TweetByScreenName, self).__init__()

    def get(self, screen_name):
        return {'Tweets': list(map(lambda x: x.json(), Tasks.find_by_screen_name(screen_name)))}  # Functional

class TweetByParty(Resource):
    parser = reqparse.RequestParser()

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('tweet_id', type=int, location='json')
        self.reqparse.add_argument('full_text', type=str, location='json')
        self.reqparse.add_argument('screen_name', type=str, location='json')
        self.reqparse.add_argument('followers_count', type=int, location='json')
        self.reqparse.add_argument('created_at', type=str, location='json')
        self.reqparse.add_argument('VADER_sentiment', type=float, location='json')
        self.reqparse.add_argument('date', type=str, location='json')
        self.reqparse.add_argument('year', type=str, location='json')
        self.reqparse.add_argument('bioguide ', type=str, location='json')
        self.reqparse.add_argument('twitter_id', type=int, location='json')
        self.reqparse.add_argument('religion', type=str, location='json')
        self.reqparse.add_argument('official_full_name', type=str, location='json')
        self.reqparse.add_argument('gender', type=str, location='json')
        self.reqparse.add_argument('birthday', type=str, location='json')
        self.reqparse.add_argument('party', type=str, location='json')
        self.reqparse.add_argument('state', type=str, location='json')
        self.reqparse.add_argument('type', type=str, location='json')


        super(TweetByParty, self).__init__()

    def get(self, party):
        return {'Tweets': list(map(lambda x: x.json(), Tasks.find_by_party(party)))}  # Functional

# Add resources and paths
api.add_resource(TaskAPI, '/api/v1.0/tweets/')

##Example of adding new ressource
api.add_resource(SingleTaskAPI, '/api/v1.0/tweets/date=<string:date>')
api.add_resource(SingleTaskByID, '/api/v1.0/tweets/id=<int:tweet_id>')
api.add_resource(TweetByState, '/api/v1.0/tweets/state=<string:state>')
api.add_resource(TweetByScreenName, '/api/v1.0/tweets/screen_name=<string:screen_name>')
api.add_resource(TweetByParty, '/api/v1.0/tweets/party=<string:party>')

with app.app_context():
    from .plotlydash.dashboard import init_dashboard
    from .plotlydash.logistic_regression_predictor import init_logistic_regression
    from .plotlydash.neural_network_predictor import init_neural_network

    app = init_dashboard(app)
    app = init_logistic_regression(app)
    app = init_neural_network(app)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=80)
