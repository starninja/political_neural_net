# Importing sqlalchemy
from flask_sqlalchemy import SQLAlchemy

# Instantiating sqlalchemy object
db = SQLAlchemy()


class BaseMixin(object):
    pass


# Creating database class
class Tasks(BaseMixin, db.Model):
    __tablename__ = 'Tweets'

    # Creating field/columns of the database as class variables
    tweet_id = db.Column(db.Integer, primary_key=True)
    full_text = db.Column(db.String(500), unique=False, nullable=True)
    screen_name = db.Column(db.String(60), unique=False, nullable=True)
    followers_count = db.Column(db.Integer, unique=False, nullable=True)
    created_at = db.Column(db.String(60), unique=False, nullable=True)
    VADER_sentiment = db.Column(db.Float, unique=False, nullable=True)
    date = db.Column(db.String(10), unique=False, nullable=True)
    year = db.Column(db.String(10), unique=False, nullable=True)
    bioguide = db.Column(db.String(20), unique=False, nullable=True)
    twitter_id = db.Column(db.Integer, unique=False, nullable=True)
    religion = db.Column(db.String(30), unique=False, nullable=True)
    official_full_name = db.Column(db.String(60), unique=False, nullable=True)
    gender = db.Column(db.String(30), unique=False, nullable=True)
    birthday = db.Column(db.String(30), unique=False, nullable=True)
    party = db.Column(db.String(30), unique=False, nullable=True)
    state = db.Column(db.String(30), unique=False, nullable=True)
    type = db.Column(db.String(30), unique=False, nullable=True)


    def __init__(self, tweet_id, full_text, screen_name, followers_count, created_at, VADER_sentiment, date, year, bioguide,
                 twitter_id, religion, official_full_name, gender, birthday, party, state, type):
        self.tweet_id = tweet_id
        self.full_text = full_text
        self.screen_name = screen_name
        self.followers_count = followers_count
        self.created_at = created_at
        self.VADER_sentiment = VADER_sentiment
        self.date = date
        self.year = year
        self.bioguide = bioguide
        self.twitter_id = twitter_id
        self.religion = religion
        self.official_full_name = official_full_name
        self.gender = gender
        self.birthday = birthday
        self.party = party
        self.state = state
        self.type = type

    # Method to show data as dictionary object
    def json(self):
        return {'tweet_id': self.tweet_id, 'full_text': self.full_text, 'screen_name': self.screen_name,
                'followers_count': self.followers_count,
                'created_at': self.created_at,
                'VADER_sentiment': self.VADER_sentiment, 'date': self.date, 'year': self.year, 'bioguide': self.bioguide, 'twitter_id': self.twitter_id,
                'religion': self.religion, 'official_full_name': self.official_full_name,
                'gender': self.gender, 'birthday': self.birthday, 'party': self.party, 'state': self.state,
                'type': self.type}

    @classmethod
    def find_by_date(cls, date):  # Revise
        return cls.query.filter_by(date=date).all()

    @classmethod
    def find_by_screen_name(cls, screen_name):  # Revise
        return cls.query.filter_by(screen_name=screen_name).all()

    @classmethod
    def find_by_id(cls, tweet_id):  # Revise
        return cls.query.filter_by(tweet_id=tweet_id).all()

    @classmethod
    def find_by_party(cls, party):  # Revise
        return cls.query.filter_by(party=party).all()

    @classmethod
    def find_by_state(cls, state):  # Revise
        return cls.query.filter_by(state=state).all()

    # Method to save data to database
    def save_to(self):
        db.session.add(self)
        db.session.commit()

    # Method to delete data from database
    def delete_(self):
        db.session.delete(self)
        db.session.commit()
