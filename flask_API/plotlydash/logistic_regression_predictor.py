import dash_core_components as dcc
import dash_html_components as html
#import dash
from dash import Dash
from dash.dependencies import Input, Output
import dash_bootstrap_components as dbc
from joblib import load

#Designs:
#Qucikstart: https://dash-bootstrap-components.opensource.faculty.ai/docs/quickstart/
#Cool setup for project: https://www.analyticsvidhya.com/blog/2020/04/how-to-deploy-machine-learning-model-flask/
#Loading screen: https://stackoverflow.com/questions/54439548/display-loading-symbol-while-waiting-for-a-result-with-plot-ly-dash

def init_logistic_regression(server):
    predictor_app = Dash(
        server=server,
        title='Logistic regression predictor',
        update_title=None,
        routes_pathname_prefix='/predict/logistic_regression/',
        external_stylesheets=[dbc.themes.BOOTSTRAP, 'style.css']
    )

    pipeline = load("flask_API/static/models/party_classification_lr.joblib")

    predictor_app.layout =html.Section([html.H1('Logistic regression predictor', style = {'margin' : '10%', 'color':'green'}),
        html.Div(dcc.Input(id="input_text",type='text', placeholder="Enter tweet for classification here", size='150'), className='predictor_input', style = {'margin' : '5%'}),
        html.Div([dbc.Button("Analyze!", color="success", className="mr-1"), dbc.Button("Clear", color="danger", className="mr-1")], style = {'margin' : '5%'}),
        html.Div(id="output", className="predictions_container"),
        html.Div(id="output_prob", className="predictions_container")
        ], className='predictor_container', style = {'text-align' : 'center'})

    @predictor_app.callback(
        Output(component_id='output', component_property='children'),
        [Input(component_id='input_text', component_property='value')]
    )
    def update_output_div(input_value):
        input_value = [input_value]

        if input_value != '':
            try:
                prediction = pipeline.predict(input_value)
                if prediction == 1:
                    prediction = 'Republican'

                if prediction == 0:
                    prediction = 'Democrat'
            except AttributeError:
                prediction = 'undetermined'

        return 'Logistic Regression predicts that the candidate is {}'.format(prediction)

    @predictor_app.callback(
        Output(component_id='output_prob', component_property='children'),
        [Input(component_id='input_text', component_property='value')]
    )
    def update_probs(input_value):
        input_value = [input_value]

        if input_value != '':
            try:
                probs = pipeline.predict_proba(input_value)

                dem_prob, rep_prob = '{}%'.format(round(float(probs[0][0])*100)), '{}%'.format(round(float(probs[0][1])*100))

            except (AttributeError) as e:
                dem_prob, rep_prob = 'undetermined', 'undetermined'

        return 'Probability of democrat is {}, while probability of republican is {}'.format(dem_prob, rep_prob)

    return predictor_app.server

