The analysis is performed in the following notebooks:
EDA:
https://colab.research.google.com/drive/1Mi7F-nrphgs3Bbw9Aw-flFp8Bfjf8erU?usp=sharing
Time series:
https://colab.research.google.com/drive/1mbCj58zpTsXlg07jpIXkIoYoAQFeyK6v?usp=sharing
NN:
https://colab.research.google.com/drive/1z7cuv7rmYZ55whZtV9rmUW0ebjw8jOOo?usp=sharing
