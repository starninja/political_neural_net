import requests
import pandas as pd

d = pd.read_csv('/Users/andreasmarkussen/Downloads/final_data.csv')
count = 0

errors = []

for tweet_id, full_text, screen_name, followers_count, created_at, retweet_text, VADER_sentiment, date, year, bioguide, twitter_id, religion, official_full_name, gender, birthday, party, state, type, is_retweet in zip(
        d['id'], d['Final text'], d['screen_name'], d['followers_count'], d['created_at'], d['retweet_text'],
        d['VADER_sentiment'],
        d['date'], d['year'], d['bioguide'], d['twitter_id'], d['religion_bio'], d['official_full_name'], d['gender_bio'], d[
            'birthday_bio'], d['party'],
        d['state'], d['type'], d['is_retweet']):
      if requests.post('http://127.0.0.1:5000/api/v1.0/challenges/27-09-2020',
                  json={'tweet_id': tweet_id, 'full_text': full_text, 'screen_name': screen_name,
                        'followers_count': followers_count,
                        'created_at': created_at,
                        'VADER_sentiment': VADER_sentiment, 'date': date, 'year': year,
                        'bioguide': bioguide,
                        'twitter_id': twitter_id, 'religion': religion,
                        'official_full_name': official_full_name, 'gender': gender,
                        'birthday': birthday, 'party': party, 'state': state, 'type': type,
                        }).status_code != 200:
            errors.append(tweet_id)


      count += 1
      if count % 1000 == 0:
            print(f'------------- Count is {count} -------------')
# pass

# Calling DataFrame constructor on list
df = pd.DataFrame(errors)
df.to_csv('errors.csv')
# This is just a post test.
quit()

r = requests.post('http://127.0.0.1:5000/api/v1.0/challenges/27-09-2020',
                  json={'tweet_id': tweet_id, 'full_text': full_text, 'screen_name': screen_name,
                        'followers_count': followers_count,
                        'created_at': created_at, 'retweet_text': retweet_text,
                        'VADER_sentiment': VADER_sentiment, 'date': date, 'year': year,
                        'bioguide': bioguide,
                        'twitter_id': twitter_id, 'religion': religion,
                        'official_full_name': official_full_name, 'gender': gender,
                        'birthday': birthday, 'party': party, 'state': state, 'type': type,
                        'is_retweet': is_retweet
                        })

print(r)
