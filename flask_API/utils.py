import requests
from flask_API.index import POST_URL

def post_to_server(tweet_id, full_text, screen_name, followers_count, created_at, retweet_text, VADER_sentiment, date, year, bioguide,
                 twitter_id, religion, official_full_name, gender, birthday, party, state, type, is_retweet):
    requests.post(POST_URL,
                  json={'tweet_id': tweet_id,
                        'full_text': full_text,
                        'screen_name': screen_name,
                        'followers_count': followers_count,
                        'created_at': created_at,
                        'retweet_text': retweet_text,
                        'VADER_sentiment': VADER_sentiment,
                        'date': date,
                        'year': year,
                        'bioguide': bioguide,
                        'twitter_id': twitter_id,
                        'religion': religion,
                        'official_full_name': official_full_name,
                        'gender': gender,
                        'birthday': birthday,
                        'party': party,
                        'state': state,
                        'type': type,
                        'is_retweet': is_retweet})


def delete_all(self):
    return requests.delete(self.all_reviews_server_url)


def delete_firm(self, id):
    return requests.delete(self.review_server_url + '/{}'.format(id))



post_to_server()
