from dash import Dash
import dash_html_components as html
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import plotly.express as px
from dateutil.relativedelta import relativedelta
import datetime as dt
import pandas as pd
#import dash
import sqlite3

import requests
from datetime import date


def init_dashboard(server):
    dash_app = Dash(
        server=server,
        title = 'Politician tweets dashboard',
        update_title = 'Loading...',
        routes_pathname_prefix='/dashboard/',
        external_stylesheets=[dbc.themes.BOOTSTRAP]
    )

    cnx = sqlite3.connect('flask_API/base.db')
    data = pd.read_sql_query('''SELECT VADER_sentiment, gender, party, screen_name, date, religion, followers_count, state, type, created_at FROM Tweets WHERE year = "2020.0" ''', cnx)
    print(data.head(5))
    #data = pd.read_csv('flask_API/base.csv')
    # the style arguments for the sidebar.
    SIDEBAR_STYLE = {
        'position': 'fixed',
        'top': 0,
        'left': 0,
        'bottom': 0,
        'width': '20%',
        'padding': '20px 10px',
        'background-color': '#f8f9fa'
    }

    # the style arguments for the main content page.
    CONTENT_STYLE = {
        'margin-left': '25%',
        'margin-right': '5%',
        'padding': '20px 10p'
    }

    TEXT_STYLE = {
        'textAlign': 'center',
        'color': '#191970'
    }

    CARD_TEXT_STYLE = {
        'textAlign': 'center',
        'color': '#0074D9'
    }

    controls = dbc.FormGroup(
        [
            html.P('Political affiliation', style={
                'textAlign': 'center'
            }),
            dcc.Dropdown(
                id='dropdown',
                options=[{
                    'label': 'Republican',
                    'value': 'republican'
                }, {
                    'label': 'Democrat',
                    'value': 'democrat'
                },
                    {
                        'label': 'Independent',
                        'value': 'independent'
                    }
                ],
                value=['republican', 'democrat'],  # default value
                multi=True
            ),
            html.Br(),
            html.P('Time range', style={
                'textAlign': 'center'
            }),
            dcc.DatePickerRange(
                id='date-picker-range',
                min_date_allowed=date(2019, 1, 1),
                max_date_allowed=date(2020, 10, 15),
                initial_visible_month=date(2020, 2, 1),
                start_date=date(2020, 1, 1),
                end_date=date(2020, 2, 1)
            ),
            html.P('Type of elected official', style={
                'textAlign': 'center'
            }),
            dbc.Card([dbc.Checklist(
                id='check_list',
                options=[{
                    'label': 'Senate',
                    'value': 'sen'
                },
                    {
                        'label': 'Congress',
                        'value': 'rep'
                    }
                ],
                value=['rep', 'sen'],
                style={
                    'margin': 'auto'
                },
                inline=True
            )]),
            html.Br(),
            html.P('Gender', style={
                'textAlign': 'center'
            }),
            dbc.Card([dbc.RadioItems(
                id='radio_items',
                options=[{
                    'label': 'Male',
                    'value': 'M'
                },
                    {
                        'label': 'Female',
                        'value': 'F'
                    },
                    {
                        'label': 'All',
                        'value': 'all'
                    }
                ],
                value='all',
                style={
                    'margin': 'auto'
                }
            )]),
            html.Br(),
            dbc.Button(
                id='submit_button',
                n_clicks=0,
                children='Submit',
                color='primary',
                block=True
            ),

        ]
    )

    sidebar = html.Div(
        [
            html.H2('Parameters', style=TEXT_STYLE),
            html.Hr(),
            controls
        ],
        style=SIDEBAR_STYLE,
    )

    content_first_row = dbc.Row([
        dbc.Col(
            dbc.Card(
                [

                    dbc.CardBody(
                        [
                            html.H4(id='card_title_1', children=['Card Title 1'], className='card-title',
                                    style=CARD_TEXT_STYLE),
                            html.P(id='card_text_1', children=['Total tweets'], style=CARD_TEXT_STYLE),
                        ]
                    )
                ]
            ),
            md=3
        ),
        dbc.Col(
            dbc.Card(
                [

                    dbc.CardBody(
                        [
                            html.H4(id='card_title_2', children=['Card Title 2'], className='card-title',
                                    style=CARD_TEXT_STYLE),
                            html.P('Unique politicians', style=CARD_TEXT_STYLE),
                        ]
                    ),
                ]

            ),
            md=3
        ),
        dbc.Col(
            dbc.Card(
                [
                    dbc.CardBody(
                        [
                            html.H4(id='card_title_3', children=['Card Title 3'], className='card-title',
                                    style=CARD_TEXT_STYLE),
                            html.P('States represented', style=CARD_TEXT_STYLE),
                        ]
                    ),
                ]

            ),
            md=3
        ),
        dbc.Col(
            dbc.Card(
                [
                    dbc.CardBody(
                        [
                            html.H4(id='card_title_4', children=['Card Title 4'], className='card-title',
                                    style=CARD_TEXT_STYLE),
                            html.P('Average followers', style=CARD_TEXT_STYLE),
                        ]
                    ),
                ]
            ),
            md=3
        )
    ])

    content_second_row = dbc.Row(
        [
            dbc.Col(
                dcc.Graph(id='graph_1',
                          config={
                              "displaylogo": False

                          }
                          ), md=6
            ),
            dbc.Col(
                dcc.Graph(id='graph_2',
                          config={
                              'displaylogo': False
                          }
                          ), md=6
            ),
        ]
    )

    content_third_row = dbc.Row(
        [
            dbc.Col(
                dcc.Graph(id='graph_4', config={
                    "displaylogo": False

                }), md=12,

            )
        ]
    )

    content_fourth_row = dbc.Row(
        [
            dbc.Col(
                dcc.Graph(id='graph_5', config={
                    "displaylogo": False

                }), md=6
            ),
            dbc.Col(
                dcc.Graph(id='graph_6', config={
                    "displaylogo": False

                }), md=6
            )
        ]
    )

    content = html.Div(
        [
            html.H2('Political Polarization Dashboard', style=TEXT_STYLE),
            html.Hr(),
            content_first_row,
            content_second_row,
            content_third_row,
            content_fourth_row,
            html.Div(id='intermediate-value', style={'display': 'none'})

        ],
        style=CONTENT_STYLE
    )

    # app = dash.Dash(external_stylesheets=[dbc.themes.BOOTSTRAP])
    dash_app.layout = html.Div([sidebar, content])

    @dash_app.callback(Output('intermediate-value', 'children'),
                       [Input('submit_button', 'n_clicks'), Input('date-picker-range', 'start_date'),
                        Input('date-picker-range', 'end_date')],
                       [State('dropdown', 'value'), State('check_list', 'value'),
                        State('radio_items', 'value')
                        ])
    def clean_data(n_clicks, start_date, end_date, dropdown_value, check_list_value, radio_items_value):
        # Filter party
        dropdown_value = [value.capitalize() for value in dropdown_value]
        if len(dropdown_value) == 2:
            filtered_data = data[(data.party == dropdown_value[0]) | (data.party == dropdown_value[1])]
        elif len(dropdown_value) == 3:
            filtered_data = data[
                (data.party == dropdown_value[0]) | (data.party == dropdown_value[1]) | (
                            data.party == dropdown_value[2])]
        elif len(dropdown_value) == 1:
            filtered_data = data[(data.party == dropdown_value[0])]
        else:
            filtered_data = data[
                (data.party == 'Independent') | (data.party == 'Republican') | (data.party == 'Democrat')]

        # Filter type
        if len(check_list_value) == 2:
            filtered_data = filtered_data[
                (filtered_data.type == check_list_value[0]) | (filtered_data.type == check_list_value[1])]
        elif len(check_list_value) == 1:
            filtered_data = filtered_data[(filtered_data.party == check_list_value[0])]
        else:
            filtered_data = filtered_data[
                (filtered_data.type != check_list_value[0]) & (filtered_data.type != check_list_value[0])]

        data.date = pd.to_datetime(data.date)

        # Filter time
        mask = (filtered_data.date >= start_date) & (filtered_data.date <= end_date)
        filtered_data = filtered_data.loc[mask]

        # Filter gender
        if radio_items_value != 'all':
            filtered_data = filtered_data[(filtered_data.gender == radio_items_value)]

        return filtered_data.to_json()

    @dash_app.callback(
        Output('graph_2', 'figure'),
        [Input('intermediate-value', 'children')])
    def update_graph_2(clean_df):
        data = pd.read_json(clean_df)
        data['hour_of_day'] = data['created_at'].dt.hour
        dem_data, rep_data = data[data.party == 'Democrat'], data[data.party == 'Republican']

        dem_hod = dem_data.groupby(['hour_of_day']).count()
        dem_hod['screen_name'] = dem_hod['screen_name'] / sum(dem_hod['screen_name']) * 100
        dem_hod['party'] = 'Democrat'
        rep_hod = rep_data.groupby(['hour_of_day']).count()
        rep_hod['screen_name'] = rep_hod['screen_name'] / sum(rep_hod['screen_name']) * 100
        rep_hod['party'] = 'Republican'

        rep_hod = rep_hod[['party', 'screen_name']]
        dem_hod = dem_hod[['party', 'screen_name']]

        data = dem_hod.append(rep_hod)
        data.reset_index(inplace=True)

        fig = px.bar(data, y='screen_name', x='hour_of_day', color='party', barmode='group')
        fig.update_layout({
            'height': 400
        })
        return fig

    @dash_app.callback(
        Output('graph_1', 'figure'),
        [Input('intermediate-value', 'children')])
    def update_graph_1(clean_df):
        data = pd.read_json(clean_df)
        data = data.groupby(by=['party', 'date']).mean()
        data.reset_index(inplace=True)

        fig = px.line(data, x="date", y="VADER_sentiment", color="party",
                      line_group="party", hover_name="party")
        fig.update_layout({
            'height': 400
        })
        return fig

    @dash_app.callback(
        Output('graph_4', 'figure'),
        [Input('intermediate-value', 'children')])
    def update_graph_4(clean_df):
        data = pd.read_json(clean_df)
        data = data.groupby(by='state').mean()
        data.reset_index(inplace=True)
        fig = px.choropleth(data,  # Input Pandas DataFrame
                            locations="state",  # DataFrame column with locations
                            color="VADER_sentiment",  # DataFrame column with color values
                            hover_name="state",  # DataFrame column hover info
                            locationmode='USA-states')  # Set to plot as US States
        fig.update_layout(
            title_text='State Rankings',  # Create a Title
            geo_scope='usa',  # Plot only the USA instead of globe
        )
        fig.update_layout({
            'height': 600
        })
        return fig

    @dash_app.callback(
        Output('graph_5', 'figure'),
        [Input('intermediate-value', 'children')])
    def update_graph_5(clean_df):
        data = pd.read_json(clean_df)
        data = data.groupby(by='religion').count()
        data.reset_index(inplace=True)
        data = data [['religion', 'screen_name']]
        fig = px.bar(data, x='screen_name', y='religion')
        return fig

    @dash_app.callback(
        Output('graph_6', 'figure'),
        [Input('intermediate-value', 'children')])
    def update_graph_6(clean_df):
        data = pd.read_json(clean_df)
        data = data.groupby(by=['party', 'screen_name']).mean()
        data.reset_index(inplace=True)
        data = data[['party', 'VADER_sentiment', 'followers_count', 'screen_name']]
        print(data)
        fig = px.scatter(data, x='VADER_sentiment', y='followers_count', color='party')
        return fig

    @dash_app.callback(
        Output('card_title_1', 'children'),
        [Input('intermediate-value', 'children')])
    def update_card_title_1(clean_df):
        data = pd.read_json(clean_df)
        return len(data)

    @dash_app.callback(
        Output('card_title_2', 'children'),
        [Input('intermediate-value', 'children')])
    def update_card_title_2(clean_df):
        data = pd.read_json(clean_df)

        return len(data.screen_name.unique())

    @dash_app.callback(
        Output('card_title_3', 'children'),
        [Input('intermediate-value', 'children')])
    def update_card_title_3(clean_df):
        data = pd.read_json(clean_df)
        return len(data.state.unique())

    @dash_app.callback(
        Output('card_title_4', 'children'),
        [Input('intermediate-value', 'children')])
    def update_card_title_4(clean_data):
        filtered = pd.read_json(clean_data)
        return str(int(round(filtered.followers_count.mean(skipna=True))))

    return dash_app.server
