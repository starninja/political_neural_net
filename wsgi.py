from flask_API.app import app
from flask_API.index import DEV_STATUS

if __name__ == '__main__':
        if not DEV_STATUS:
                app.run()
        else:
                app.run(debug=True)
