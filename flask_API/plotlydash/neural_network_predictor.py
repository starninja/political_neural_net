import dash_core_components as dcc
import dash_html_components as html
#import dash
from dash import Dash
from dash.dependencies import Input, Output
import dash_bootstrap_components as dbc
import dash_table
import pandas as pd
from joblib import load

from keras.models import Sequential
from keras.layers import Embedding, LSTM, Dense
from keras.optimizers import Adam
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from flask_API.predictor_model import CONTRACTION_MAPPING, clean_text
import re

#Designs:
#Qucikstart: https://dash-bootstrap-components.opensource.faculty.ai/docs/quickstart/
#Cool setup for project: https://www.analyticsvidhya.com/blog/2020/04/how-to-deploy-machine-learning-model-flask/
#Loading screen: https://stackoverflow.com/questions/54439548/display-loading-symbol-while-waiting-for-a-result-with-plot-ly-dash

def init_neural_network(server):
    nn_app = Dash(
        server=server,
        title='Neural network predictor',
        update_title=None,
            routes_pathname_prefix='/predict/neural_network/',
        external_stylesheets=[dbc.themes.BOOTSTRAP, 'style.css']
    )

    m = Sequential()
    m.add(Embedding(input_dim = 57448, output_dim = 128, input_length = 132))
    m.add(LSTM(128))
    m.add(Dense(64, activation='relu'))
    m.add(Dense(2, activation='softmax'))
    m.compile(loss='binary_crossentropy', optimizer='adam', metrics=['binary_accuracy'])
    m.load_weights("flask_API/static/models/party_classification_nn.h5" )

    tokenizer = Tokenizer(lower=True)

    pipeline_nb = load("flask_API/static/models/party_classification_lr.joblib")
    pipeline_rf = load("flask_API/static/models/party_classification_rf.joblib")

    nn_app.layout =html.Section([html.H1('Neural network predictor', style = {'margin' : '5%', 'color':'green'}),
        html.Div(dcc.Input(id="input_text",type='text', placeholder="Enter tweet for classification here", size='150'), className='predictor_input', style = {'margin' : '2%'}),
        html.Div(id="output", className="predictions_container"),
        html.Div(id="output_prob", className="predictions_container"),
        html.Div(id="output_table", className="predictions_container", style={"margin" : "10px", 'width' : '800px', 'margin-top': '50px',
    'margin-left': 'auto',
    'margin-right': 'auto'})

        ], className='predictor_container', style = {'text-align' : 'center'})

    @nn_app.callback(
    Output("example-output", "children"), [Input("example-button", "n_clicks")])
    def on_button_click(n):
        if n is None:
            
            return "Not clicked."
        else:
            return f"Clicked {n} times."

    @nn_app.callback(
        Output(component_id='output', component_property='children'),
        [Input(component_id='input_text', component_property='value')]
    )
    def update_output_div(input_value):
        input_value = [input_value]

        if input_value != '':
            prediction = 'error'
            input_value = [clean_text(text, CONTRACTION_MAPPING) for text in input_value]  # Only one input in this case
            print(input_value)
            tokenizer.fit_on_texts(input_value)
            words = input_value
            input_value = tokenizer.texts_to_sequences(input_value)
            input_value = pad_sequences(input_value, maxlen = 132)
            prediction = m.predict(input_value)

            print(input_value)
            prediction = prediction.argmax(axis=1)
            if prediction == 1:
               prediction = 'Republican'

            if prediction == 0:
               prediction = 'Democrat'
        #except AttributeError:
            #prediction = 'undetermined'

        return 'Neural network predicts that the candidate is {}'.format(prediction)

    @nn_app.callback(
        Output(component_id='output_prob', component_property='children'),
        [Input(component_id='input_text', component_property='value')]
    )
    def update_probs(input_value):
        input_value = [input_value]

        if input_value != '':
            try:
                input_value = [clean_text(text, CONTRACTION_MAPPING) for text in input_value]  # Only one input in this case
                tokenizer.fit_on_texts(input_value)
                words = input_value
                input_value = tokenizer.texts_to_sequences(input_value)
                input_value = pad_sequences(input_value, maxlen=132)

                probs = m.predict(input_value)

                dem_prob, rep_prob = '{}%'.format(round(float(probs[0][0])*100)), '{}%'.format(round(float(probs[0][1])*100))

            except (AttributeError) as e:
                dem_prob, rep_prob = 'undetermined', 'undetermined'


    @nn_app.callback(
        Output(component_id='output_table', component_property='children'),
        [Input(component_id='input_text', component_property='value')]
    )
    def update_probs(input_value):
        input_value = [input_value]

        if input_value != '':
            prediction = 'error'
            input_value = [clean_text(text, CONTRACTION_MAPPING) for text in input_value]  # Only one input in this case
            print(input_value)
            tokenizer.fit_on_texts(input_value)
            words = input_value
            input_value = tokenizer.texts_to_sequences(input_value)
            input_value = pad_sequences(input_value, maxlen = 132)
            probs = m.predict(input_value)

            print(input_value)
            prediction = probs.argmax(axis=1)
            if prediction == 1:
               prediction_party = 'Republican'

            if prediction == 0:
               prediction_party = 'Democrat'

            nb_probs = pipeline_nb.predict_proba(words)
            nb_prediction = pipeline_nb.predict(words)
            if nb_prediction == 1:
                nb_prediction = 'Republican'

            if nb_prediction == 0:
                nb_prediction = 'Democrat'

            rf_probs = pipeline_rf.predict_proba(words)
            rf_prediction = pipeline_rf.predict(words)
            if rf_prediction == 1:
                rf_prediction = 'Republican'

            if rf_prediction == 0:
                rf_prediction = 'Democrat'

            nn_dem_prob, nn_rep_prob = '{}%'.format(round(float(probs[0][0])*100)), '{}%'.format(round(float(probs[0][1])*100))
            nb_dem_prob, nb_rep_prob = '{}%'.format(round(float(nb_probs[0][0])*100)), '{}%'.format(round(float(nb_probs[0][1])*100))
            rf_dem_prob, rf_rep_prob = '{}%'.format(round(float(rf_probs[0][0])*100)), '{}%'.format(round(float(rf_probs[0][1])*100))
            


        df = pd.DataFrame()


        df['Model type:'] = ['Predicted party affiliation:', 'Probability democrat:', 'Probability republican:']
        df['Neural Network'] = [prediction_party,  nn_dem_prob, nn_rep_prob]
        df['Naive Bayes'] = [nb_prediction,  nb_dem_prob, nb_rep_prob]
        df['Random Forest'] = [rf_prediction,  rf_dem_prob, rf_rep_prob]

        return dbc.Table.from_dataframe(df, striped=True, bordered=True, hover=True, responsive=True)

    return nn_app.server

